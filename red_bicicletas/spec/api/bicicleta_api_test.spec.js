var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server= require('../../bin/www');
beforeEach(() => {Bicicleta.allBicis=[];});
beforeEach(() => {console.log('testeando...')});



describe('Bicicleta API',() => {

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'conecction error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });
    
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });


    describe('GET BICICLETAS /',() =>{
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana', [-0.186643, -78.479188]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });

        });
    });


    describe('DELETE BICICLETA /create',() =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -14, "lng": -34 }';

            request.post({
                headers: headers,
                url: "http://localhost:5000/api/bicicletas/create",
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();         
                          
            });

        });
    });

    describe('POST BICICLETA /delete',() =>{
        it('Status 204', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -14, "lng": -34 }';

            request.delete({
                headers: headers,
                url: "http://localhost:5000/api/bicicletas/delete",
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.removeById(10)).toBe();
                done();         
                          
            });

        });
    });

    describe('PUT BICICLETA /update/:id',() =>{
        it('Status 200', (done) => {

            var a = new Bicicleta(1, 'negro', 'urbana', [-0.186643, -78.479188]);
            Bicicleta.add(a);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 1, "color": "rojo", "modelo": "urbana", "lat": -14, "lng": -34 }';            

            request.put({
                headers: headers,
                url: "http://localhost:5000/api/bicicletas/update/:1",
                body: aBici


            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();         
                          
            });

        });
    });

});

